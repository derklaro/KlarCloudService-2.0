/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.examples.logging;

import de.klarcloudservice.logging.KlarCloudConsoleLogger;
import de.klarcloudservice.logging.enums.AnsiColourHandler;

import java.io.IOException;

/**
 * @author _Klaro | Pasqual K. / created on 27.12.2018
 */

public class KlarCloudConsoleLoggerImplementationExample extends KlarCloudConsoleLogger {
    /**
     * Creates a new instance of the {@link KlarCloudConsoleLogger}
     *
     * @param colour
     * @throws IllegalAccessException
     * @throws NoSuchFieldException
     * @throws IOException
     */

    /**
     * Creates a new KlarCloudConsole Logger instance
     */
    public KlarCloudConsoleLoggerImplementationExample(boolean colour) throws IOException {
        super(colour);
    }

    /**
     * Prints a info String
     */
    @Override
    public void info(String message) {
        super.info(message);
    }

    @Override
    public void err(String message) {
        try {
            this.getConsoleReader() //Returns the ConsoleReader of the Cloud
                    .println(AnsiColourHandler.toColouredString("§cI am red §rI have the default colour, because of a reset :("
                            + "§eI am yellow")); //Like Bukkit and BungeeCord colour codes
            this.complete(); //Needed
        } catch (final IOException ignored) {
        }

        super.err(message);
    }
}
