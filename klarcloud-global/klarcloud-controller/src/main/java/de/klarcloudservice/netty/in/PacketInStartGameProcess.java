/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.in;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.meta.server.ServerGroup;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.netty.packets.PacketOutStartGameServer;
import de.klarcloudservice.utility.TypeTokenAdaptor;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 16.12.2018
 */

public class PacketInStartGameProcess implements NettyAdaptor {
    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
        final ServerGroup serverGroup = configuration.getValue("group", TypeTokenAdaptor.getServerGroupType());
        if (!KlarCloudController.getInstance().getChannelHandler().isChannelRegistered(serverGroup.getClient()))
            return;

        final Collection<String> servers = KlarCloudController.getInstance().getInternalCloudNetwork()
                .getServerProcessManager().getOnlineServers(serverGroup.getName());
        final Collection<String> waiting = KlarCloudController.getInstance().getCloudProcessOfferService().getWaiting(serverGroup.getName());

        final int waitingAndOnline = servers.size() + waiting.size();
        final String id = KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().nextFreeServerID(serverGroup.getName());
        final String name = serverGroup.getName() + KlarCloudController.getInstance().getCloudConfiguration().getSplitter() + (Integer.parseInt(id) <= 9 ? "0" : "") + id;

        if (serverGroup.getMaxOnline() > waitingAndOnline || serverGroup.getMaxOnline() == -1)
            KlarCloudController.getInstance().getChannelHandler().sendPacketAsynchronous(serverGroup.getClient(),
                    new PacketOutStartGameServer(serverGroup, name, UUID.randomUUID(), configuration.getConfiguration("pre"), id)
            );
    }
}
