package de.klarcloudservice.netty.in;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.enums.QueryType;

import java.util.List;

public class PacketInServerDisable implements NettyAdaptor {

    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
        String serverName = configuration.getStringValue("serverName");
        KlarCloudController.getInstance().getChannelHandler().closeChannel(serverName);
    }
}
