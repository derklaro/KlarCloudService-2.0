/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.in;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.event.enums.EventTargetType;
import de.klarcloudservice.event.events.ProcessRegisterEvent;
import de.klarcloudservice.meta.info.ProxyInfo;
import de.klarcloudservice.meta.info.ServerInfo;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.netty.packets.PacketOutProcessAdd;
import de.klarcloudservice.utility.TypeTokenAdaptor;

import java.util.List;

/**
 * @author _Klaro | Pasqual K. / created on 11.11.2018
 */

public class PacketInAddProcess implements NettyAdaptor {
    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
        if (configuration.contains("serverInfo")) {
            final ServerInfo serverInfo = configuration.getValue("serverInfo", TypeTokenAdaptor.getServerInfoType());

            ProcessRegisterEvent processRegisterEvent = new ProcessRegisterEvent(false, true, false, serverInfo.getCloudProcess().getName());
            KlarCloudController.getInstance().getEventManager().callEvent(EventTargetType.PROCESS_REGISTERED, processRegisterEvent);

            if (processRegisterEvent.isCancelled())
                return;

            KlarCloudController.getInstance().getCloudProcessOfferService().getWaiting().remove(serverInfo.getCloudProcess().getName());
            KlarCloudController.getInstance().getChannelHandler().sendToAllAsynchronous(new PacketOutProcessAdd(serverInfo));
        } else {
            final ProxyInfo proxyInfo = configuration.getValue("proxyInfo", TypeTokenAdaptor.getProxyInfoType());

            ProcessRegisterEvent processRegisterEvent = new ProcessRegisterEvent(false, true, false, proxyInfo.getCloudProcess().getName());
            KlarCloudController.getInstance().getEventManager().callEvent(EventTargetType.PROCESS_REGISTERED, processRegisterEvent);

            if (processRegisterEvent.isCancelled())
                return;

            KlarCloudController.getInstance().getCloudProcessOfferService().getWaiting().remove(proxyInfo.getCloudProcess().getName());
            KlarCloudController.getInstance().getChannelHandler().sendToAllAsynchronous(new PacketOutProcessAdd(proxyInfo));
        }
    }
}
