/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice;

import de.klarcloudservice.event.EventManager;
import de.klarcloudservice.logging.KlarCloudConsoleLogger;
import de.klarcloudservice.utility.cloudsystem.InternalCloudNetwork;
import lombok.Getter;
import lombok.Setter;

import javax.management.InstanceAlreadyExistsException;

/**
 * @author _Klaro | Pasqual K. / created on 19.10.2018
 */

@Getter
@Setter
public class KlarCloudLibraryService {
    @Getter
    private static KlarCloudLibraryService instance;
    private InternalCloudNetwork internalCloudNetwork;

    private EventManager eventManager;

    private KlarCloudConsoleLogger klarCloudConsoleLogger;
    private String key, controllerIP;

    /**
     * Creates a new Instance of the {KlarCloudLibraryService}
     *
     * @param klarCloudConsoleLogger
     * @param key
     * @param controllerIP
     * @param eventManager
     * @throws Throwable
     */
    public KlarCloudLibraryService(KlarCloudConsoleLogger klarCloudConsoleLogger, String key, String controllerIP, EventManager eventManager) throws Throwable {
        if (instance == null)
            instance = this;
        else
            throw new InstanceAlreadyExistsException();

        this.key = key;
        this.controllerIP = controllerIP;
        this.klarCloudConsoleLogger = klarCloudConsoleLogger;
        this.eventManager = eventManager;
    }
}
