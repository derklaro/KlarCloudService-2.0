/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.channel;

import de.klarcloudservice.KlarCloudLibraryService;
import de.klarcloudservice.event.enums.EventTargetType;
import de.klarcloudservice.event.events.IncomingPacketEvent;
import de.klarcloudservice.event.events.PacketHandleSuccessEvent;
import de.klarcloudservice.netty.NettyHandler;
import de.klarcloudservice.netty.authentication.AuthenticationHandler;
import de.klarcloudservice.netty.packet.Packet;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.utility.AccessChecker;
import de.klarcloudservice.utility.TypeTokenAdaptor;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.AllArgsConstructor;

import java.io.IOException;
import java.net.InetSocketAddress;

/**
 * @author _Klaro | Pasqual K. / created on 18.10.2018
 */

@AllArgsConstructor
public class ChannelReader extends SimpleChannelInboundHandler {
    private NettyHandler nettyHandler;
    private ChannelHandler channelHandler;

    @Override
    protected void channelRead0(final ChannelHandlerContext channelHandlerContext, final Object object) {
        if (!(object instanceof Packet))
            return;

        Packet packet = (Packet) object;

        IncomingPacketEvent incomingPacketEvent = new IncomingPacketEvent(packet, false);
        KlarCloudLibraryService.getInstance().getEventManager().callEvent(EventTargetType.INCOMING_PACKET, incomingPacketEvent);

        if (incomingPacketEvent.isCancelled())
            return;

        if (!packet.getQueryTypes().contains(QueryType.COMPLETE))
            return;

        packet.getQueryTypes().remove(QueryType.COMPLETE);
        packet.getConfiguration().addProperty("responseUUID", packet.getResponseUUID());

        final String address = ((InetSocketAddress) channelHandlerContext.channel().remoteAddress()).getAddress().getHostAddress();

        if (packet.getType().equalsIgnoreCase("InitializeCloudNetwork") && KlarCloudLibraryService.getInstance().getControllerIP() != null
                && new AccessChecker().checkString(KlarCloudLibraryService.getInstance().getControllerIP(), address).isAccepted()) {
            channelHandler.registerChannel("KlarCloudController", channelHandlerContext);
        }

        if (!new AccessChecker().checkChannel(channelHandler.getChannelList(), channelHandlerContext).isAccepted()) {
            if (!packet.getType().equalsIgnoreCase("Auth") || !packet.getConfiguration().contains("AuthenticationType")) {
                channelHandlerContext.channel().close();
                return;
            }

            new AuthenticationHandler().handleAuth(packet.getConfiguration().getValue("AuthenticationType", TypeTokenAdaptor.getAuthenticationType()),
                    packet, channelHandlerContext, channelHandler);
        }

        if (channelHandler.getPackets().getOrDefault(packet.getResponseUUID(), null) != null) {
            channelHandler.getPackets().get(packet.getResponseUUID()).set(packet);
        }

        if (nettyHandler.handle(packet.getType(), packet.getConfiguration(), packet.getQueryTypes()))
            KlarCloudLibraryService.getInstance().getEventManager().callEvent(EventTargetType.PACKET_HANDLE_SUCCESS, new PacketHandleSuccessEvent(true, packet));
        else
            KlarCloudLibraryService.getInstance().getEventManager().callEvent(EventTargetType.PACKET_HANDLE_SUCCESS, new PacketHandleSuccessEvent(false, packet));
    }

    @Override
    public void channelInactive(final ChannelHandlerContext ctx) {
        final InetSocketAddress inetSocketAddress = ((InetSocketAddress) ctx.channel().remoteAddress());
        if (!ctx.channel().isActive() && !ctx.channel().isOpen() && !ctx.channel().isWritable() && inetSocketAddress != null) {
            KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().info("Channel [Host=" + inetSocketAddress.getAddress().getHostAddress() + "/Port=" + inetSocketAddress.getPort() + "] is disconnected");
        }
    }

    @Override
    public void exceptionCaught(final ChannelHandlerContext ctx, final Throwable cause) {
        if (!(cause instanceof IOException)) {
            KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().err("An exceptionCaught() event was fired, and it reached at the tail of the pipeline. It usually means the last handler in the pipeline did not handle the exception.");
            cause.printStackTrace();
        }
    }
}
