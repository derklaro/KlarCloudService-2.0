/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.authentication.enums;

/**
 * @author _Klaro | Pasqual K. / created on 19.10.2018
 */

public enum AuthenticationType {
    PROXY,
    SERVER,
    INTERNAL,
    WEB
}
