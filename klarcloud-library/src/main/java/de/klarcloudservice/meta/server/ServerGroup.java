/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.server;

import de.klarcloudservice.meta.Template;
import de.klarcloudservice.meta.enums.ServerModeType;
import de.klarcloudservice.meta.server.configuration.AdvancedConfiguration;
import de.klarcloudservice.meta.server.versions.SpigotVersions;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author _Klaro | Pasqual K. / created on 21.10.2018
 */

@AllArgsConstructor
@Getter
@Setter
public class ServerGroup implements Serializable {
    private static final long serialVersionUID = -6849497313084944255L;

    protected String name, client, motd, join_permission;
    protected int memory, minOnline, maxOnline, startPort, percentOfOnline;

    protected boolean maintenance;
    protected ServerModeType serverModeType;

    protected AdvancedConfiguration advancedConfiguration;

    protected Template template;
    protected SpigotVersions spigotVersions;
}
