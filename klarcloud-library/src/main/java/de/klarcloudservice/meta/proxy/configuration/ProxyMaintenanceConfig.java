package de.klarcloudservice.meta.proxy.configuration;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
public final class ProxyMaintenanceConfig implements Serializable {
    private static final long serialVersionUID = 6690669721709901553L;

    private Motd maintenanceMode;
    private String maintenanceProtocol;
    private List<String> maintenancePlayerInfo;
}
