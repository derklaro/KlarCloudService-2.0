/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.info;

import de.klarcloudservice.meta.CloudProcess;
import de.klarcloudservice.meta.server.ServerGroup;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 29.10.2018
 */

@AllArgsConstructor
@Getter
@Setter
public class ServerInfo implements Serializable {
    private static final long serialVersionUID = 8057730391607929124L;

    private CloudProcess cloudProcess;

    private ServerGroup serverGroup;

    private String group, host, motd;
    private int port, online, maxMemory;

    @Deprecated
    private boolean maintenance;
    private boolean ingame;

    private List<UUID> onlinePlayers;
}
