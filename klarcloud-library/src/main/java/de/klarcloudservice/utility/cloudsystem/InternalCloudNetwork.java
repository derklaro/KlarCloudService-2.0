/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.utility.cloudsystem;

import de.klarcloudservice.KlarCloudLibrary;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.meta.client.Client;
import de.klarcloudservice.meta.proxy.ProxyGroup;
import de.klarcloudservice.meta.server.ServerGroup;
import de.klarcloudservice.meta.web.InternalWebUser;
import de.klarcloudservice.utility.StringUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * @author _Klaro | Pasqual K. / created on 20.10.2018
 */

@Data
public class InternalCloudNetwork implements Serializable {
    private static final long serialVersionUID = 4564917986901138765L;

    private int webPort = 4790;

    private boolean signs = true;
    private InternalWebUser internalWebUser = new InternalWebUser(KlarCloudLibrary.THREAD_LOCAL_RANDOM.nextLong(0, Long.MAX_VALUE) + "-internal", KlarCloudLibrary.THREAD_LOCAL_RANDOM.nextLong(0, Long.MAX_VALUE) + StringUtil.EMPTY + KlarCloudLibrary.THREAD_LOCAL_RANDOM.nextLong(0, Long.MAX_VALUE));

    private Configuration messages;

    private String prefix = StringUtil.NULL;

    public String getMessage(final String key) {
        return this.messages.getStringValue(key).replaceAll("%prefix%", this.prefix);
    }

    private final ServerProcessManager serverProcessManager = new ServerProcessManager();

    private Map<String, ServerGroup> serverGroups = KlarCloudLibrary.concurrentHashMap();
    private Map<String, ProxyGroup> proxyGroups = KlarCloudLibrary.concurrentHashMap();
    private Map<String, Client> clients = KlarCloudLibrary.concurrentHashMap();
}
