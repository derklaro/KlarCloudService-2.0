/*
  Copyright © 2019 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.versioneering;

import com.google.gson.stream.JsonReader;
import de.klarcloudservice.KlarCloudLibrary;
import de.klarcloudservice.KlarCloudLibraryService;
import de.klarcloudservice.utility.StringUtil;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;

/**
 * @author _Klaro | Pasqual K. / created on 08.01.2019
 */

final class VersionLoader {
    static String getNewestVersion() {
        try {
            URLConnection urlConnection = new URL("https://dl.klarcloudservice.de/update/internal/version.json").openConnection();
            urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
            urlConnection.setUseCaches(false);
            urlConnection.connect();

            try (JsonReader jsonReader = new JsonReader(new InputStreamReader(urlConnection.getInputStream(), StandardCharsets.UTF_8))) {
                return KlarCloudLibrary.PARSER.parse(jsonReader).getAsJsonObject().get("version").getAsString();
            }
        } catch (final IOException ex) {
            StringUtil.printError(KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger(), "Error while checking newest version", ex);
        }
        return StringUtil.NULL;
    }
}
