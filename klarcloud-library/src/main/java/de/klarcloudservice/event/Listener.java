/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.event;

import de.klarcloudservice.event.enums.EventTargetType;
import de.klarcloudservice.event.events.*;
import lombok.Getter;

/**
 * @author _Klaro | Pasqual K. / created on 27.12.2018
 */

@Getter
public abstract class Listener {
    public Listener(String name, EventTargetType eventTargetType) {
        this.name = name;
        this.eventTargetType = eventTargetType;
    }

    private String name;
    private EventTargetType eventTargetType;

    public void handle(LoadSuccessEvent event) {
    }

    public void handle(PacketHandleSuccessEvent event) {
    }

    public void handle(IncomingPacketEvent event) {
    }

    public void handle(OutGoingPacketEvent event) {
    }

    public void handle(ProcessRegisterEvent event) {
    }

    public void handle(ProcessUnregistersEvent event) {
    }
}
